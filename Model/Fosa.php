<?php

namespace FacturaScripts\Plugins\CementerioCrematorioMascotas\Model;

use FacturaScripts\Core\Model\Base;
use FacturaScripts\Core\Base\DataBase\DataBaseWhere;
use FacturaScripts\Plugins\CementerioCrematorioMascotas\Model\FosaMascota;
use FacturaScripts\Plugins\CementerioCrematorioMascotas\Model\FosaCliente;
use FacturaScripts\Core\Model\Variante;

class Fosa extends Base\ModelClass
{
    use Base\ModelTrait;
    
    public $fosacodigo;
    public $fosaestado;
    public $fosagps;
    
    public static function primaryColumn()
    {
        return 'fosacodigo';
    }

    public static function tableName()
    {
        return 'fosas';
    }
    
    public function liberarFosa($codes)
    {
        $fosa = new Fosa();
        
        foreach ($codes as $code) {
            $fosa->clear();
            
            if ($fosa->loadFromCode($code)) {
                $fosa->fosaestado = 0;
                
                if ($fosa->save()) {
                    $this->liberarFosaCliente($fosa->fosacodigo);
                }
            }
        }
    }
    
    public function liberarFosaCliente($fosacodigo)
    {
        $fosaCliente = new FosaCliente();
        $where = [
            new DataBaseWhere('fosacodigo', $fosacodigo),
            new DataBaseWhere('fcfechasalida', null, 'IS')
        ];
        $clientes = $fosaCliente->all($where);
        
        foreach ($clientes as $cliente) {
            $fosaCliente->clear();
            
            if ($fosaCliente->loadFromCode($cliente->fcid)) {
                $fosaCliente->fcfechasalida = date('d-m-Y');
                
                if ($fosaCliente->save()) {
                    $this->liberarFosaMascota($fosacodigo);
                }
            }
        }
    }
    
    public function liberarFosaMascota($fosacodigo)
    {
        $fosaMascota = new FosaMascota();
        $where = [
            new DataBaseWhere('fosacodigo', $fosacodigo),
            new DataBaseWhere('fmfechasalida', null, 'IS')
        ];
        $mascotas = $fosaMascota->all($where);
        
        foreach ($mascotas as $mascota) {            
            if ($fosaMascota->loadFromCode($mascota->fmid)) {
                $fosaMascota->fmfechasalida = date('d-m-Y');
                $fosaMascota->save();
            }
        }
    }
    
    public function asociarCliente($data)
    {
        $codes = $data['codes'];
        $codcliente = $data['codcliente'];
        
        $this->liberarFosa($codes);
        
        $fosa = new Fosa();
        $fosaCliente = new FosaCliente();
        
        foreach ($codes as $code) {
            $fosa->clear();
            
            if ($fosa->loadFromCode($code)) {
                $fosa->fosaestado = 2;

                if ($fosa->save()) {
                    $fosaCliente->clear();
                    $fosaCliente->fosacodigo = $code;
                    $fosaCliente->codcliente = $codcliente;
                    $fosaCliente->precio = 24.79;
                    
                    if (isset($data['fcfechaentrada'])) {
                        $fosaCliente->fcfechaentrada = $data['fcfechaentrada'];
                    } else {
                        $fosaCliente->fcfechaentrada = date('Y-m-d');
                    }
                    
                    if (isset($data['fcfechacaducidad'])) {
                        $fosaCliente->fcfechacaducidad = $data['fcfechacaducidad'];
                    } else {
                        $fosaCliente->fcfechacaducidad = date('Y-m-d');
                    }
                    
                    $fosaCliente->save();
                }
            }
        }
    }
    
    public function asociarMascotaFosa($data)
    {
        $FosaMascota = new FosaMascota();
        $FosaMascota->fosacodigo = $data['fosacodigo'];
        $FosaMascota->mascotaid = $data['mascotaid'];
        $FosaMascota->fmfechaentrada = $data['fmfechaentrada'];
        return $FosaMascota->save();
    }
    
    public function save() {
        $result = parent::save();
        
        if ($result) {
            $posicion_coincidencia = strpos($_SERVER['HTTP_HOST'], 'localhost');
            if ($posicion_coincidencia === false) {
                $this->update_map_web();
            }
        }
        
        return $result;
    }
    
    public function update_map_web()
    {
        $fosa = new Fosa();
        $where = [
            new DataBaseWhere('fosacodigo', $this->fosacodigo)
        ];
        $fosa->loadFromCode('', $where);
        
        if ($fosa->fosagps != '') {
            $gps = array_map('trim', explode(',', $fosa->fosagps));
            
            $marker = array(
                'map_id' => 4,
                'address' => $fosa->fosagps,
                'title' => $fosa->fosacodigo,
                'lat' => $gps[0],
                'lng' => $gps[1],
                'approved' => 1,
                'description' => '',
                'icon' => 'https://www.elisia.es/wp-content/uploads/2019/10/punto-fosa-libre.png'
            );

            if ($fosa->fosaestado == 1) {
                $marker['icon'] = 'https://www.elisia.es/wp-content/uploads/2019/10/punto-fosa-reservada.png';
            } else if ($fosa->fosaestado == 2) {
                $marker['icon'] = 'https://www.elisia.es/wp-content/uploads/2019/10/punto-fosa-ocupada.png';
            }
            
            $modelFosaMascota = new FosaMascota();
            $where = [
                new DataBaseWhere('fosacodigo', $fosa->fosacodigo),
                new DataBaseWhere('fmfechasalida', null, 'IS')
            ];
            $FosaMacotas = $modelFosaMascota->all($where);

            foreach ($FosaMacotas as $FosaMacota) {
                $Mascota = new Mascota();
                $where = [
                    new DataBaseWhere('mascotaid', $FosaMacota->mascotaid)
                ];
                $Mascota->loadFromCode('', $where);
                $marker['description'] = $marker['description'].$Mascota->mascotanombre.', ';
            }

            if ($marker['description'] != '') {
                $marker['description'] = substr($marker['description'], 0, -2);
            }
            
            $wpDB = mysqli_connect("localhost", "elisiadb", "V15u4lc0d3", "elisiadb");
            $wpDB->set_charset("utf8");

            if (!$wpDB) {
                die("Connection failed: " . mysqli_connect_error());
            }
            
            if ($fosa->fosaestado == 3) {
                $query = "DELETE FROM wp_wpgmza WHERE title='".$marker['title']."' AND map_id=4";
            } else {
                $select = "SELECT * FROM wp_wpgmza WHERE title ='".$marker['title']."'";
                $resultSELECT = mysqli_query($wpDB, $select);

                if ($resultSELECT->num_rows == 0) {
                    $query = "INSERT INTO wp_wpgmza (map_id, address, title, lat, lng, approved, description, icon) VALUES ('" . $marker['map_id'] . "', '" . $marker['address'] . "', '" . $marker['title'] . "', '" . $marker['lat'] . "', '" . $marker['lng'] . "', '" . $marker['approved'] . "', '" . $marker['description'] . "', '" . $marker['icon'] . "')";
                } else {
                    $query = "UPDATE wp_wpgmza SET description='".$marker['description']."', icon='".$marker['icon']."' WHERE title='".$marker['title']."' AND map_id=4";
                }
            }

            mysqli_query($wpDB, $query);

            mysqli_close($wpDB);
        }  
    }
}