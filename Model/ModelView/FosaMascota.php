<?php

namespace FacturaScripts\Plugins\CementerioCrematorioMascotas\Model\ModelView;

use FacturaScripts\Core\Model\Base\JoinModel;

class FosaMascota extends JoinModel
{    
    protected function getFields(): array {
        return [
            'fmid' => 'fosas_mascotas.fmid',
            'fosacodigo' => 'fosas_mascotas.fosacodigo',
            'mascotaid' => 'fosas_mascotas.mascotaid',
            'fosas_mascotas' => 'fosas_mascotas.fosacodigo',
            'fmfechaentrada' => 'fosas_mascotas.fmfechaentrada',
            'fmfechasalida' => 'fosas_mascotas.fmfechasalida',
            'codcliente' => 'mascotas.codcliente',
        ];
    }

    protected function getSQLFrom(): string {        
        return 'fosas_mascotas'
            . ' INNER JOIN mascotas ON mascotas.mascotaid = fosas_mascotas.mascotaid';
    }

    protected function getTables(): array {
        return [
            'fosas_mascotas',
            'mascotas'
        ];
    }
}