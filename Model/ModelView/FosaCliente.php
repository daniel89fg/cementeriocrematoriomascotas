<?php

namespace FacturaScripts\Plugins\CementerioCrematorioMascotas\Model\ModelView;

use FacturaScripts\Core\Model\Base\JoinModel;

class FosaCliente extends JoinModel
{    
    protected function getFields(): array {
        return [
            'fcid' => 'fosas_clientes.fcid',
            'fosacodigo' => 'fosas_clientes.fosacodigo',
            'codcliente' => 'fosas_clientes.codcliente',
            'fcfechaentrada' => 'fosas_clientes.fcfechaentrada',
            'fcfechasalida' => 'fosas_clientes.fcfechasalida',
            'fcfechacaducidad' => 'fosas_clientes.fcfechacaducidad',
			'precio' => 'fosas_clientes.precio',
			'anosrestantes' => 'fosas_clientes.anosrestantes'
        ];
    }

    protected function getSQLFrom(): string {        
        return 'fosas_clientes';
    }

    protected function getTables(): array {
        return [
            'fosas_clientes'
        ];
    }
}