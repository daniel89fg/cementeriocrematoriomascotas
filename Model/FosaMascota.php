<?php

namespace FacturaScripts\Plugins\CementerioCrematorioMascotas\Model;

use FacturaScripts\Core\Model\Base;

class FosaMascota extends Base\ModelClass
{
    use Base\ModelTrait;

    public $fmid;
    public $mascotaid;
    public $fosacodigo;
    public $fmfechaentrada;
    public $fmfechasalida;
    
    public static function primaryColumn()
    {
        return 'fmid';
    }

    public static function tableName()
    {
        return 'fosas_mascotas';
    }
    
    public function clear()
    {
        parent::clear();
        $this->fmfechaentrada = date('d-m-Y');
    }
}