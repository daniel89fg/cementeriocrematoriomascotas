<?php

namespace FacturaScripts\Plugins\CementerioCrematorioMascotas\Model;

use FacturaScripts\Core\Model\Base;

class Mascota extends Base\ModelClass
{
    use Base\ModelTrait;
    
    public $mascotaid;
    public $codcliente;
    public $codagente;
    public $mascotanombre;
    public $mascotapeso;
    public $mascotaraza;
    public $mascotasexo;
    public $mascotacausamuerte;
    public $mascotachip;
    public $mascotafechaalta;
    public $mascotaanonacimiento;
    public $mascotaincineracion;
    
    
    public static function primaryColumn()
    {
        return 'mascotaid';
    }
    
    public function primaryDescriptionColumn()
    {
        return 'mascotanombre';
    }

    public static function tableName()
    {
        return 'mascotas';
    }
    
    public function clear()
    {
        parent::clear();
        $this->mascotafechaalta = date('d-m-Y');
    }
}
