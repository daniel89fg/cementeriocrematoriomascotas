<?php

namespace FacturaScripts\Plugins\CementerioCrematorioMascotas\Model;

use FacturaScripts\Core\Model\Base;
use FacturaScripts\Core\Model\Producto;
use FacturaScripts\Core\Model\Variante;
use FacturaScripts\Core\Base\DataBase\DataBaseWhere;

class FosaCliente extends Base\ModelClass
{
    use Base\ModelTrait;
    
    public $fcid;
    public $fosacodigo;
    public $codcliente;
    public $fcfechaentrada;
    public $fcfechasalida;
    public $fcfechacaducidad;
    public $idfactura;
    public $anosrestantes;
    public $precio;
    
    public static function primaryColumn()
    {
        return 'fcid';
    }

    public static function tableName()
    {
        return 'fosas_clientes';
    }
    
    public function clear()
    {
        parent::clear();        
        $this->fcfechaentrada = date('d-m-Y');
        $this->fcfechacaducidad = '01-01-'.(date('Y')+1);
        $this->anosrestantes = 5;
        $this->precio = 24.79;
    }
}