<?php
namespace FacturaScripts\Plugins\CementerioCrematorioMascotas;

use FacturaScripts\Core\Base\InitClass;
use FacturaScripts\Plugins\CementerioCrematorioMascotas\Model\Fosa;
use FacturaScripts\Plugins\CementerioCrematorioMascotas\Model\Mascota;
use FacturaScripts\Plugins\CementerioCrematorioMascotas\Model\FosaCliente;
use FacturaScripts\Plugins\CementerioCrematorioMascotas\Model\FosaMascota;

class Init extends InitClass
{

    public function init()
    {
        $this->loadExtension(new Extension\Controller\EditCliente());
        $this->loadExtension(new Extension\Controller\EditAgente());
        $this->loadExtension(new Extension\Model\ReciboCliente());
    }

    public function update()
    {
        new Fosa();
        new Mascota();
        new FosaCliente();
        new FosaMascota();
    }
}