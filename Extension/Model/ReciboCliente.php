<?php

namespace FacturaScripts\Plugins\CementerioCrematorioMascotas\Extension\Model;

use FacturaScripts\Plugins\CementerioCrematorioMascotas\Model\FosaCliente;
use FacturaScripts\Core\Base\DataBase\DataBaseWhere;

class ReciboCliente
{
    public function save()
    {
        return function() {
            if ($this->pagado) {                
                $fosaCliente = new FosaCliente();
                $where = [new DataBaseWhere('idfactura', $this->idfactura)];
                $fosaCliente->loadFromCode('', $where);

                if ($fosaCliente) {
                    $anoActual = date("Y");
                    $siguienteAno = date("Y", strtotime($anoActual."+ 1 year"));
                    $fosaCliente->fcfechacaducidad = $siguienteAno.'-01-01';
                    $fosaCliente->idfactura = NULL;
                    $fosaCliente->anosrestantes = $fosaCliente->anosrestantes - 1;
                    $fosaCliente->save();
                }
            }
        };
    }
}