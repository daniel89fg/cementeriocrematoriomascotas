<?php
namespace FacturaScripts\Plugins\CementerioCrematorioMascotas\Extension\Controller;

use FacturaScripts\Core\Base\DataBase\DataBaseWhere;

class EditCliente
{
    public function createViews()
    {
        return function() {
            $this->addListView('ListMascota', 'Mascota', 'mascotas', 'fas fa-kiwi-bird');
            $this->views['ListMascota']->disableColumn('cliente', true);
            $this->setSettings('ListMascota', 'btnDelete', false);
        };
    }
    
    public function loadData()
    {
        return function($viewName, $view) {
            if ($viewName == 'ListMascota') {
                $mainViewName = $this->getMainViewName();
                $codcliente = $this->getViewModelValue($mainViewName, 'codcliente');
                $where = [new DataBaseWhere('codcliente', $codcliente)];
                $order = ['mascotaid' => 'ASC'];
                $view->loadData('', $where, $order);
            }
        };
    }
}