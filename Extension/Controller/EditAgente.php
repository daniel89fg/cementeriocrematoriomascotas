<?php
namespace FacturaScripts\Plugins\CementerioCrematorioMascotas\Extension\Controller;

use FacturaScripts\Core\Base\DataBase\DataBaseWhere;

class EditAgente
{
    public function createViews()
    {
        return function() {
            $this->addListView('ListMascota', 'Mascota', 'mascotas', 'fas fa-kiwi-bird');
            $this->views['ListMascota']->disableColumn('agent', true);
            $this->setSettings('ListMascota', 'btnDelete', false);
        };
    }
    
    public function loadData()
    {
        return function($viewName, $view) {
            if ($viewName == 'ListMascota') {
                $mainViewName = $this->getMainViewName();
                $codagente = $this->getViewModelValue($mainViewName, 'codagente');
                $where = [new DataBaseWhere('codagente', $codagente)];
                $order = ['mascotaid' => 'ASC'];
                $view->loadData('', $where, $order);
            }
        };
    }
}