<?php

namespace FacturaScripts\Plugins\CementerioCrematorioMascotas\Controller;

use FacturaScripts\Core\Lib\ExtendedController;

class EditMascota extends ExtendedController\EditController
{
    public function getModelClassName()
    {
        return 'Mascota';
    }

    public function getPageData()
    {
        $pagedata = parent::getPageData();
        $pagedata['title'] = 'editar-mascota';
        $pagedata['menu'] = 'cementerio';
        $pagedata['icon'] = 'fas fa-edit';
        $pagedata['showonmenu'] = false;

        return $pagedata;
    }
    
    protected function createViews() {
        parent::createViews();
        $this->setSettings('EditMascota', 'btnDelete', false);
    }
}