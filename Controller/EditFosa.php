<?php

namespace FacturaScripts\Plugins\CementerioCrematorioMascotas\Controller;

use FacturaScripts\Core\Base\DataBase\DataBaseWhere;
use FacturaScripts\Core\Lib\ExtendedController;
use FacturaScripts\Plugins\CementerioCrematorioMascotas\Model\Fosa;
use FacturaScripts\Plugins\CementerioCrematorioMascotas\Model\FosaCliente;
use FacturaScripts\Plugins\CementerioCrematorioMascotas\Model\FosaMascota;
use FacturaScripts\Plugins\CementerioCrematorioMascotas\Model\Mascota;

class EditFosa extends ExtendedController\PanelController
{

    public function getPageData()
    {
        $pagedata = parent::getPageData();
        $pagedata['title'] = 'editar-fosa';
        $pagedata['menu'] = 'cementerio';
        $pagedata['icon'] = 'fas fa-edit';
        $pagedata['showonmenu'] = false;

        return $pagedata;
    }
    
    protected function createViews() {
        $this->addEditView('EditFosa', 'Fosa', 'fosa', 'fas fa-tree');
        $this->setSettings('EditFosa', 'btnDelete', false);
        
        $this->addEditListView('ListFosaCliente', 'FosaCliente', 'customers', 'fas fa-users');
        $this->views['ListFosaCliente']->disableColumn('fosa', true);
        
        $this->addEditListView('ListFosaMascota', 'FosaMascota', 'mascotas', 'fas fa-kiwi-bird');
        $this->views['ListFosaMascota']->disableColumn('fosa', true);
        $this->views['ListFosaMascota']->disableColumn('customer', true);
        
        $FosaCliente = new FosaCliente();
        $fosacodigo = $this->request->get('code');
        $where = [
            new DataBaseWhere('fosacodigo', $fosacodigo),
            new DataBaseWhere('fcfechasalida', NULL),
        ];
        $FosaCliente->loadFromCode('', $where);
        
        $customValues = [];
        if ($FosaCliente) {
            $modelMascotasCliente = new Mascota();
            $where = [
                new DataBaseWhere('codcliente', $FosaCliente->codcliente),
                new DataBaseWhere('mascotaincineracion', 0)
            ];
            $MascotasCliente = $modelMascotasCliente->all($where);
            
            if ($MascotasCliente) {
                foreach ($MascotasCliente as $MascotaCliente) {
                    $FosaMascota = new FosaMascota();
                    $where = [
                        new DataBaseWhere('mascotaid', $MascotaCliente->mascotaid),
                        new DataBaseWhere('fmfechasalida', NULL)
                    ];
                    if (!$FosaMascota->loadFromCode('', $where)) {
                        $aux = [
                            'value' => $MascotaCliente->mascotaid,
                            'title' => $MascotaCliente->mascotanombre
                        ];
                        array_push($customValues, $aux);
                    }
                }
            }
        }

        $columnToModify = $this->views['ListFosaMascota']->columnForName('mascotas');
        $columnToModify->widget->setValuesFromArray($customValues);
    }
    
    protected function loadData($viewName, $view)
    {
        switch ($viewName) {            
            case 'ListFosaCliente':
                $fosacodigo = $this->request->get('code');
                $where = [new DataBaseWhere('fosacodigo', $fosacodigo)];
                $order = ['fcid' => 'DESC'];
                $view->loadData('', $where, $order);
                break;
            
            case 'ListFosaMascota':
                $fosacodigo = $this->request->get('code');
                $where = [new DataBaseWhere('fosacodigo', $fosacodigo)];
                $order = ['fmid' => 'DESC'];
                $view->loadData('', $where, $order);
                break;
            
            default:
                $code = $this->request->get('code');
                $view->loadData($code);
                break;
        }
    }
    
    protected function execPreviousAction($action)
    {
        $activetab = $this->request->request->get('activetab');
        
        switch ($action) {
            case 'insert':
                switch ($activetab) {
                    case 'EditFosa':
                        $aux = new Fosa();
                        $aux->fosaestado = 0;
                        $aux->save();
                        break;

                    case 'ListFosaCliente':
                        $data = array(
                            'codes' => [$this->request->query->get('code')],
                            'codcliente' => $this->request->request->get('codcliente'),
                            'fcfechaentrada' => $this->request->request->get('fcfechaentrada'),
                            'fcfechacaducidad' => $this->request->request->get('fcfechacaducidad'),
                            'precio' => $this->request->request->get('precio')
                        );

                        if ($data['codes']) {
                            $aux = new Fosa();
                            $result = $aux->asociarCliente($data);
                        }
                        break;
                        
                    case 'ListFosaMascota':
                        $FosaMascota = array(
                            'fosacodigo' => $this->request->query->get('code'),
                            'mascotaid' => $this->request->request->get('mascotaid'),
                            'fmfechaentrada' => $this->request->request->get('fmfechaentrada')
                        );
                        $Fosa = new Fosa();
                        $result = $Fosa->asociarMascotaFosa($FosaMascota);
                        break;
                }
                break;
            
            case 'delete':
                switch ($activetab) {                        
                    case 'ListFosaMascota':
                        $FosaMascota = new FosaMascota();
                        $FosaMascota->loadFromCode($this->request->request->get('code'));
                        $FosaMascota->fmfechasalida = date('d-m-Y');
                        $result = $FosaMascota->save();
                        break;
                    
                    case 'ListFosaCliente':                        
                        $Fosa = new Fosa();
                        $codes = array($this->request->query->get('code'));
                        $result = $Fosa->liberarFosa($codes);
                        break;
                }
                break;
                         
            default:
                $result = parent::execPreviousAction($action);
                break;
        }
        
        return $result;
    }
}