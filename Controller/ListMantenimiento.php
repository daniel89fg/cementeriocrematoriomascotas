<?php
namespace FacturaScripts\Plugins\CementerioCrematorioMascotas\Controller;

use FacturaScripts\Core\Lib\ExtendedController;
use FacturaScripts\Core\Lib\BusinessDocumentTools;
use FacturaScripts\Core\Base\DataBase\DataBaseWhere;
use FacturaScripts\Core\Model\FacturaCliente;
use FacturaScripts\Core\Model\Variante;
use FacturaScripts\Plugins\CementerioCrematorioMascotas\Model\FosaCliente;
use FacturaScripts\Plugins\CementerioCrematorioMascotas\Model\Fosa;
use FacturaScripts\Core\Model\Cliente;

class ListMantenimiento extends ExtendedController\ListController
{    
    public function getPageData()
    {
        $pageData = parent::getPageData();
        $pageData['title'] = 'mantenimientos';
        $pageData['icon'] = 'fas fa-history';
        $pageData['menu'] = 'cementerio';

        return $pageData;
    }
    
    protected function createViews()
    {
        //FOSAS MASCOTAS
        $tabMantenimiento = 'ListMantenimiento';
        $this->addView($tabMantenimiento, 'FosaCliente', 'mantenimientos', 'fas fa-history');
        
        $btnFacturar = [
            'action' => 'btnFacturar',
            'color' => 'warning',
            'icon' => 'fas fa-copy',
            'label' => 'facturar',
            'type' => 'action'
        ];
        $this->addButton($tabMantenimiento, $btnFacturar);
        
        $this->setSettings($tabMantenimiento, 'btnDelete', false);
        $this->setSettings($tabMantenimiento, 'btnNew', false);
        
        $this->addOrderBy($tabMantenimiento, ['fcfechacaducidad' ,'fcid'], 'fechacaducidad', 2);
        
        $this->addSearchFields($tabMantenimiento, ['fosacodigo']);
        $this->addFilterAutocomplete($tabMantenimiento, 'codcliente', 'customer', 'fosas_clientes.codcliente', 'clientes', 'codcliente', 'nombre');
        $this->addFilterAutocomplete($tabMantenimiento, 'idfactura', 'invoice', 'idfactura', 'facturascli', 'idfactura', 'codigo');
        $this->addFilterPeriod($tabMantenimiento, 'fcfechaentrada', 'fechaentrada', 'fcfechaentrada');
        $this->addFilterPeriod($tabMantenimiento, 'fcfechacaducidad', 'fechacaducidad', 'fcfechacaducidad');
    }
    
    protected function loadData($viewName, $view)
    {
        switch ($viewName) {
            case 'ListMantenimiento':
                $where = [
                    new DataBaseWhere('fcfechasalida', NULL),
                    new DataBaseWhere('fcfechacaducidad', Date('Y-m-d'), '<='),
                    new DataBaseWhere('anosrestantes', 0, '>')
                ];
                
                $view->loadData('', $where);
                break;

            default:
                parent::loadData($viewName, $view);
                break;
        }
    }
    
    protected function execPreviousAction($action)
    {
        switch ($action) {
            case 'btnFacturar':
                $codes = $this->request->request->get('code');
                
                if ($codes) {
                    $fosaCliente = new FosaCliente();
                    $Fosa = new Fosa();
                    $cliente = new Cliente();
                    $factura = new FacturaCliente();
                    
                    foreach ($codes as $code) {
                        $fosaCliente->clear();
                        $Fosa->clear();
                        
                        $fosaCliente->loadFromCode($code);
                        
                        if (is_null($fosaCliente->idfactura)) {
                            $cliente->clear();
                            $Fosa->loadFromCode($fosaCliente->fosacodigo);
                            $cliente->loadFromCode($fosaCliente->codcliente);

                            $factura->clear();
                            $factura->setSubject($cliente);

                            if($factura->save()) {
                                $fosaCliente->idfactura = $factura->idfactura;
                                $fosaCliente->save();

                                $newLinea = $factura->getNewProductLine('');
                                $newLinea->cantidad = 1;
                                $newLinea->pvpunitario = $fosaCliente->precio;
                                $newLinea->descripcion = 'Mantenimiento anual '.date("Y").' fosa '.$fosaCliente->fosacodigo;
                                $newLinea->save();
                                /// recalculamos
                                $docTools = new BusinessDocumentTools();
                                $docTools->recalculate($factura);
                                $result['result'] = $factura->save();
                            }
                        }
                    }
                }
                
                return true;
        }

        return parent::execPreviousAction($action);
    }
}