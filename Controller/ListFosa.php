<?php
namespace FacturaScripts\Plugins\CementerioCrematorioMascotas\Controller;

use FacturaScripts\Core\Lib\ExtendedController;
use FacturaScripts\Plugins\CementerioCrematorioMascotas\Model\Fosa;

class ListFosa extends ExtendedController\ListController
{    
    public function getPageData()
    {
        $pageData = parent::getPageData();
        $pageData['title'] = 'fosas';
        $pageData['icon'] = 'fas fa-tree';
        $pageData['menu'] = 'cementerio';

        return $pageData;
    }
    
    protected function createViews()
    {
        //FOSAS
        $tabFosa = 'ListFosa';
        $this->addView($tabFosa, 'Fosa', 'fosas', 'fas fa-tree');
        $this->setSettings($tabFosa, 'btnDelete', false);
        $this->setSettings($tabFosa, 'btnNew', false);
        
        $this->addSearchFields($tabFosa, ['fosacodigo']);
        $this->addOrderBy($tabFosa, ['fosacodigo'], 'codigo', 2);
        
        $estados = [
            ['code' => '', 'description' => '------'],
            ['code' => '0', 'description' => $this->toolBox()->i18n()->trans('libre')],
            ['code' => '1', 'description' => $this->toolBox()->i18n()->trans('reservado')],
            ['code' => '2', 'description' => $this->toolBox()->i18n()->trans('ocupado')],
            ['code' => '3', 'description' => $this->toolBox()->i18n()->trans('deshabilitado')]
        ];
        $this->addFilterSelect($tabFosa, 'fosaestado', 'estado', 'fosaestado', $estados);

        $this->addFilterCheckbox($tabFosa, 'fosagps', 'falta-gps', 'fosagps', 'IS', null);
                
        /// BOTONES
        $addFosa = [
            'action' => 'addFosa',
            'color' => 'success',
            'icon' => 'fas fa-plus',
            'label' => 'new',
            'type' => 'action'
        ];
        $this->addButton($tabFosa, $addFosa);

        $liberarFosa = [
            'action' => 'liberarFosa',
            'color' => 'warning',
            'icon' => 'fas fa-sign-out-alt',
            'label' => 'liberar-fosa',
            'type' => 'action'
        ];
        $this->addButton($tabFosa, $liberarFosa);
        
        //FOSAS CLIENTES
        $tabFosaCliente = 'ListFosaCliente';
        $this->addView($tabFosaCliente, 'ModelView\FosaCliente', 'fosas-clientes', 'fas fa-users');
        $this->setSettings($tabFosaCliente, 'btnDelete', false);
        $this->setSettings($tabFosaCliente, 'btnNew', false);
        $this->setSettings($tabFosaCliente, 'checkBoxes', false);
        
        $this->addOrderBy($tabFosaCliente, ['fcfechaentrada', 'fcid'], 'fechaentrada', 2);
        $this->addOrderBy($tabFosaCliente, ['fcfechacaducidad', 'fcid'], 'fechacaducidad');
        $this->addOrderBy($tabFosaCliente, ['fcfechasalida', 'fcid'], 'fechasalida');
        
        $this->addSearchFields($tabFosaCliente, ['fosacodigo']);
        $this->addFilterAutocomplete($tabFosaCliente, 'codcliente', 'customer', 'codcliente', 'clientes', 'codcliente', 'nombre');
        $this->addFilterPeriod($tabFosaCliente, 'fcfechaentrada', 'fechaentrada', 'fcfechaentrada');
        $this->addFilterPeriod($tabFosaCliente, 'fcfechacaducidad', 'fechacaducidad', 'fcfechacaducidad');
        $this->addFilterPeriod($tabFosaCliente, 'fcfechasalida', 'fechasalida', 'fcfechasalida');
        $this->addFilterCheckbox($tabFosaCliente, 'fcfechacaducidad', 'show-caducados', 'fcfechacaducidad', '<', date('Y-m-d'));

        //FOSAS MASCOTAS
        $tabFosaMascota = 'ListFosaMascota';
        $this->addView($tabFosaMascota, 'ModelView\FosaMascota', 'fosas-mascotas', 'fas fa-kiwi-bird');
        $this->setSettings($tabFosaMascota, 'btnDelete', false);
        $this->setSettings($tabFosaMascota, 'btnNew', false);
        $this->setSettings($tabFosaMascota, 'checkBoxes', false);
        
        $this->addOrderBy($tabFosaMascota, ['fmfechaentrada' ,'fmid'], 'fechaentrada', 2);
        $this->addOrderBy($tabFosaMascota, ['fmfechasalida', 'fmid'], 'fechasalida');
        
        $this->addSearchFields($tabFosaMascota, ['fosacodigo']);
        $this->addFilterAutocomplete($tabFosaMascota, 'codcliente', 'customer', 'codcliente', 'clientes', 'codcliente', 'nombre');
        $this->addFilterAutocomplete($tabFosaMascota, 'mascotaid', 'mascota', 'mascotas.mascotaid', 'mascotas', 'mascotaid', 'mascotanombre');
        $this->addFilterPeriod($tabFosaMascota, 'fmfechaentrada', 'fechaentrada', 'fmfechaentrada');
        $this->addFilterPeriod($tabFosaMascota, 'fmfechasalida', 'fechasalida', 'fmfechasalida');
    }
    
    protected function execPreviousAction($action)
    {
        switch ($action) {            
            case 'liberarFosa':
                $codes = $this->request->request->get('code');
                if ($codes) {
                    $aux = new Fosa();
                    $aux->liberarFosa($codes);
                }
                return true;
            
            case 'addFosa':
                $aux = new Fosa();
                $aux->fosaestado = 0;

                if ($aux->save()) {
                    $this->toolBox()->i18nLog()->notice('record-updated-correctly');
                    return true;
                } else {
                    $this->toolBox()->i18nLog()->error('record-save-error');
                    return false;
                }
                
            default:
                return parent::execPreviousAction($action);
        }
    }
}