<?php
namespace FacturaScripts\Plugins\CementerioCrematorioMascotas\Controller;

use FacturaScripts\Core\Lib\ExtendedController;

class ListMascota extends ExtendedController\ListController
{
    public function getPageData()
    {
        $pageData = parent::getPageData();
        $pageData['title'] = 'mascotas';
        $pageData['icon'] = 'fas fa-kiwi-bird';
        $pageData['menu'] = 'cementerio';

        return $pageData;
    }

    protected function createViews() {
        $tabMascota = 'ListMascota';
        $this->addView($tabMascota, 'Mascota', 'mascotas', 'fas fa-kiwi-bird');
        $this->addSearchFields($tabMascota, ['mascotanombre', 'mascotaraza', 'mascotachip']);
        $this->addOrderBy($tabMascota, ['mascotaid'], 'fechaalta', 2);
        $this->addOrderBy($tabMascota, ['codcliente'], 'customer');
        $this->addOrderBy($tabMascota, ['codagente'], 'agent');
        $this->addOrderBy($tabMascota, ['mascotaincineracion'], 'incineracion');
		$this->addOrderBy($tabMascota, ['mascotapeso'], 'peso');
        
        $this->setSettings($tabMascota, 'btnDelete', false);
        
        $this->addFilterAutocomplete($tabMascota, 'codcliente', 'customer', 'codcliente', 'clientes', 'codcliente', 'nombre');
        
        $this->addFilterAutocomplete($tabMascota, 'codagente', 'agent', 'codagente', 'agentes', 'codagente', 'nombre');
        
        $filterIncineracion = [
            ['code' => '', 'description' => '------'],
            ['code' => '0', 'description' => $this->toolBox()->i18n()->trans('no')],
            ['code' => '1', 'description' => $this->toolBox()->i18n()->trans('colectiva')],
            ['code' => '2', 'description' => $this->toolBox()->i18n()->trans('individual')],
        ];
        $this->addFilterSelect($tabMascota, 'mascotaincineracion', 'incineracion', 'mascotaincineracion', $filterIncineracion);
        
        $this->addFilterPeriod($tabMascota, 'mascotafechaalta', 'fechaalta', 'mascotafechaalta');
		
		$this->addFilterNumber($tabMascota, 'min-peso', 'min-peso', 'mascotapeso');
		$this->addFilterNumber($tabMascota, 'max-peso', 'max-peso', 'mascotapeso', '<=');
    }
}