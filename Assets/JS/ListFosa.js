$(document).ready(function(){
    $('#asocCliente').click(function(){
        var codes = '';
        
        $('#ListFosa .table-responsive tbody tr').each(function(){
            var input = $(this).find('td:first input[type="checkbox"]');
            
            if ($(input).prop('checked')) {
                codes = codes + $(input).val() + '|';
            }
        });
        
        codes = codes.substring(0,codes.length-1);
        $('input[name="codes"]').val(codes);
    });
});